var fs = require("fs");
var path = require("path");
var currentDirectoryPath = getCurrentDirectoryPath();
var getlist = function (dir, done) {
  var results = [];
  fs.readdir(dir, function (err, list) {
    if (err) return done(err);
    var i = 0;
    (function next() {
      var file = list[i++];
      if (!file) return done(null, results);
      file = path.resolve(dir, file);
      fs.stat(file, function (err, stat) {
        if (stat && stat.isDirectory()) {
          getlist(file, function (err, res) {
            results = results.concat(res);
            next();
          });
        } else {
          fs.readFile(file, function (err, data) {
            if (err) throw err;
            if (data.includes('TODO')) {
              console.log(file);
              next();
            }
          });
          next();
        }
      });
    })();
  });
};
getlist(currentDirectoryPath, function (err, results) {
  if (err) throw err;
});

function getCurrentDirectoryPath() {
  var currentPath = path.dirname(__filename).split(path.sep)
  var currentPathlength = currentPath.length - 1;
  var result;
  currentPath.forEach(function (currentPath, index, array) {
    if (index == 0) {
      result = currentPath;
    }
    else if (index < currentPathlength) {
      result = result + "/" + currentPath;
    }
  });
  return result;
}